<?php
/**
 * The template for displaying the header
 *
 * @package sitenamehere
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
    
</head>

<body>

<div class="wrap">
  <?php if ( has_nav_menu( 'sitenamehere' ) ) : ?>
    <?php
        // Primary navigation menu.
        wp_nav_menu( array(
          'menu_class'     => 'nav nav-bar',
          'theme_location' => 'sitenamehere',
        ) );
      ?>
  <?php endif; ?>