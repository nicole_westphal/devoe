<?php
/**
 * The template for displaying the footer
 *
 * @package sitenamehere
 */
?>
    <footer >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
                    <?php dynamic_sidebar( 'footer-1' ); ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>

<?php wp_footer(); ?>
</body>
</html>
