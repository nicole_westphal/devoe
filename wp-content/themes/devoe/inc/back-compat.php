<?php
/**
 * Twenty Fifteen back compat functionality
 *
 * @package sitenamehere
 */


function sitenamehere_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'sitenamehere_upgrade_notice' );
}
add_action( 'after_switch_theme', 'sitenamehere_switch_theme' );


function sitenamehere_upgrade_notice() {
	$message = sprintf( __( 'Twenty Fifteen requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'sitenamehere' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

function sitenamehere_customize() {
	wp_die( sprintf( __( 'Twenty Fifteen requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'sitenamehere' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'sitenamehere_customize' );


function sitenamehere_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Twenty Fifteen requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'sitenamehere' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'sitenamehere_preview' );
