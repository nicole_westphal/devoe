<?php
/**
 * The template for displaying pages
 *
 * @package sitenamehere
 */

get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="primary-main">
                <?php if ( have_posts() ) { ?><?php while ( have_posts() ) : the_post(); if($post->post_content!=""){ ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php } endwhile; ?><?php } ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>