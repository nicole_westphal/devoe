<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package sitenamehere
 */

get_header(); ?>

	<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <section class="error-404 not-found">
			        <header class="page-header">
				        <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'sitenamehere' ); ?></h1>
			        </header><!-- .page-header -->

			        <div class="page-content">
				        <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'sitenamehere' ); ?></p>

				        <?php get_search_form(); ?>
			        </div><!-- .page-content -->
		        </section><!-- .error-404 -->
            </div>
        </div>
    </div>

<?php get_footer(); ?>
